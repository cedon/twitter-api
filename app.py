#!/usr/bin/env python3

import requests
import os
import json
from flask import Flask, request, redirect, url_for, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_modus import Modus


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgres://postgres:"+os.environ['POSTGRES_PASSWORD']+"@db/"+os.environ['POSTGRES_DB']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = "False"
modus = Modus(app)
db = SQLAlchemy(app)

class Post(db.Model):

	__tablename__ = "posts"

	id = db.Column(db.Integer, primary_key=True)
	twitter_id = db.Column(db.Text)
	text = db.Column(db.Text)

	def __init__(self, twitter_id, text):
		self.twitter_id = twitter_id
		self.text = text

db.create_all() # criar banco de dados da aplicação

#def auth():
#	return os.environ.get("BEARER_TOKEN")

def create_url():
	query = "%23openbanking"
	max_results = "100"
	url = "https://api.twitter.com/2/tweets/search/recent?query={}&max_results={}".format(
		query, max_results
	)
	return url

def create_headers(bearer_token):
	headers = {"Authorization": "Bearer {}".format(bearer_token)}
	return headers

def connect_to_endpoint(url, headers):
	response = requests.request("GET", url, headers=headers)
	print(response.status_code)
	if response.status_code != 200:
		raise Exception(response.status_code, response.text)
	return response.json()

@app.route('/')
def root():
	return redirect(url_for('index'))

@app.route('/posts', methods=['GET', 'POST'])
def index():
	if(request.method == 'POST'):
		new_post = Post(request.form['twitter_id'], request.form['text'])
		db.session.add(new_post)
		db.session.commit()
		return redirect(url_for('index'))
#	bearer_token = "AAAAAAAAAAAAAAAAAAAAAPcOMAEAAAAARbOB9bhbJlIHIKw41fsLki8I%2FDo%3DtH3Gf84IbD4dgVTYO1u7krsAz2iVGRWQLfasoGNuhZXAABENSJ"
#	bearer_token = auth()
#	url = create_url()
#	headers = create_headers(bearer_token)
#	json_response = connect_to_endpoint(url, headers)
#	print(json.dumps(json_response, indent=4, sort_keys=True))
#	return render_template('index.html', posts=print(json.dumps(json_response, indent=4, sort_keys=True)))
	return render_template('index.html', posts=Post.query.all())
#	return render_template('index.html', posts=jsonify(json_response))

@app.route('/posts/new')
def new():
	return render_template('new.html')

#@app.route('/posts/search')
#def search():
#	return render_template('search.html')

@app.route('/posts/<int:id>', methods=['GET', 'PATCH', 'DELETE'] )
def show(id):
	found_post = Post.query.get(id)
	if (request.method == b'PATCH'):
		found_post.twitter_id = request.form['twitter_id']
		found_post.text = request.form['text']
		db.session.add(found_post)
		db.session.commit()
		return redirect(url_for('index'))
	if (request.method == b'DELETE'):
		db.session.delete(found_post)
		db.session.commit()
		return redirect(url_for('index'))
	return render_template('show.html', post=found_post)

@app.route('/posts/<int:id>/edit')
def edit(id):
	found_post = Post.query.get(id)
	return render_template('edit.html', post=found_post)

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True, port=3000)
