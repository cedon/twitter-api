import requests
import os
import json
from flask import Flask, request, redirect, url_for, jsonify

# To set your enviornment variables in your terminal run the following line:
# export 'BEARER_TOKEN'='<your_bearer_token>'

app = Flask(__name__)


def auth():
    return os.environ.get("BEARER_TOKEN")


def create_url(query):
    #query = searchq
    # Tweet fields are adjustable.
    # Options include:
    # attachments, author_id, context_annotations,
    # conversation_id, created_at, entities, geo, id,
    # in_reply_to_user_id, lang, non_public_metrics, organic_metrics,
    # possibly_sensitive, promoted_metrics, public_metrics, referenced_tweets,
    # source, text, and withheld
    max_results = "100"
    tweet_fields = "tweet.fields=author_id,lang,public_metrics,source,text,withheld"
    url = "https://api.twitter.com/2/tweets/search/recent?query={}&max_results={}&{}".format(
        query, max_results, tweet_fields
    )
    return url


def create_headers(bearer_token):
    headers = {"Authorization": "Bearer {}".format(bearer_token)}
    return headers


def connect_to_endpoint(url, headers):
    response = requests.request("GET", url, headers=headers)
    print(response.status_code)
    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()

@app.route('/')
def root():
        return redirect(url_for('main'))

@app.route('/api')
def main():
    queryl = ['%23openbanking','%23remediation','%23devops','%23sre,%23microservices','%23observability','%23oauth','%23metrics','%23logmonitoring','%23opentracing']
    bearer_token = auth()
    for x in queryl:
        url = create_url(x)
        headers = create_headers(bearer_token)
        json_response = connect_to_endpoint(url, headers)
        return json_response
#        return jsonify(json.dumps(json_response, indent=4, sort_keys=True))


#if __name__ == "__main__":
#    main()

if __name__ == '__main__':
        app.run(host='0.0.0.0', debug=True, port=5000)

