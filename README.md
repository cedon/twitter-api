# Curso Python3

## About

    - Twitter API(Incompleto) 
    - Tem uma api que consulta hashtags específicas e retorna o valor dos 100 ultimos tweets.
    - Utiliza Falsk, Docker, Docker Compose. 
    - Está com frontend incompleto, porém preparado para adaptação 
    - para interagir como um CRUD com uma base PostgreSQL
    - Também está configurado com uma stack de monitoramento baseada no prometheus 
    - e grafana para coletar dados da stack
    - Os dados configurados atualmente(arquivo prometheus.yaml) 
    - estão coletando informações do host do serviço de Docker 

## Usage
    - prometheus.yaml - arquivo de configuração do prometheus
    - alert.rules - arquivo de definição de regras de alerta
    - alertmanager.yaml - arquivo de configuração do alertmanager
    - para que o projeto rode, o arquivo .env deve ser configurado com as variáveis:
    - BEARER_TOKEN(bearer token gerado para sua API do twitter)
    - POSTGRES_PASSWORD(senha do usuário postgres)
    - POSTGRES_DB(nome da base que será utilizada)
    - para testar a api `curl ip_do_host:5000/api`

## Requisitos
    - Docker 20.10.x
    - Docker-Compose 1.27.x

## Tutorial
    - na pasta raiz do repositório executar:
    - editar o arquivo .env e acrescentar as credenciais
    - depois, executar o comando `docker-compose up -d`

## Acesso no navegador

    - Link api http://ip_do_host:5000  
    - Link frontend http://ip_do_host:3000
    - Link grafana http://ip_do_host:3030
    - Link prometheus http://ip_do_host:9090
    - Link alertmanager http://ip_do_host:9093

